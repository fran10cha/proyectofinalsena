export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyCBMkEblh65pIL0OLdaEbR7iTjWepQ0EPc',
    authDomain: 'testapp-78720.firebaseapp.com',
    databaseURL: 'https://testapp-78720.firebaseio.com',
    projectId: 'testapp-78720',
    storageBucket: 'testapp-78720.appspot.com',
    messagingSenderId: '581815912803',
    appId: '1:581815912803:web:c1daada924e99540b8ab56',
    measurementId: 'G-BNE1R0L5DB'
  }
};
